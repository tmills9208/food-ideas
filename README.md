How to install / use
====================

1. Download / Clone the code from the repository to a directory of your choosing

2. Open the index.html on a web browser

License
=======

This project is under the MIT License for free fair use. This license was chosen because this isnt a big idea at the moment with little idea of how to properly capitalize on it, that and I would need to use a different, more dedicated api or system for requesting dishes. 